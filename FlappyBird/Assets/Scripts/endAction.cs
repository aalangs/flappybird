﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class endAction : MonoBehaviour
{
    Vector2 movement = new Vector2(0, 5);
    private GameObject bird;
    private float rotationSpeed = 10;
    AudioSource soundDie;
    // Start is called before the first frame update
    void Start()
    {
        bird = GameObject.FindGameObjectWithTag("bird");
        GetComponent<Rigidbody2D>().velocity = movement;	
        Destroy(bird.GetComponent<touchAction>());
        Destroy(bird.GetComponent<collideManagementBird>());
        soundDie = GameObject.FindGameObjectWithTag("soundDie").GetComponent<AudioSource>();
        soundDie.Play();
    }

    // Update is called once per frame
    void Update()
    {
        //Rotation de bird après de toucher le pipe
        bird.transform.Rotate(new Vector3(0f, 0f, 100f * rotationSpeed * Time.deltaTime));

        //Si le bird a disparu de l'écran on le destruit et on lance la scene Game Over
        if (!gameObject.GetComponent<SpriteRenderer> ().isVisible)
        {
            Destroy(bird);
            //On envoi true à la variable pour recommencer le affiche de la monnaie
            Variable.setRandomBox(true);
            SceneManager.LoadScene("scene4-Game Over");
        }
    }
}
