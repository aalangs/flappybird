﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variable : MonoBehaviour
{

    private static Variable instance = null;
    public static bool randomBox = true;
    public static float score = 0;
    public static float bestScore = 0;

    void Awake(){
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public static bool getRandomBox(){
        return randomBox;
    }

    public static void setRandomBox(bool boolean){
        randomBox = boolean;
    }

    public static float getScore(){
        return score;
    }

    public static void setScore(float newScore){
        score = newScore;
    }

    //On fait la comparaison du score obtenu avec le Best Score, si c'est plus grand ça veut dire que c'est le New Best Score
    public static bool compareScore(float compare){
        if (compare > bestScore)
        {
            //On change le valeur du Best Score
            setBestScore(compare);
            return true;
        }
        return false;
    }

    public static float getBestScore(){
        return bestScore;
    }

    public static void setBestScore(float newBest){
        bestScore = newBest;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
