﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour
{
    public Vector2 movement;
    public GameObject pipe1Up;
    public GameObject pipe1Down;
    public GameObject box1;
    public GameObject box2;
    public Vector3 siz;
    private Transform pipe1UpOriginalTransform;
    private Transform pipe1DownOriginalTransform;
    public Vector3 leftBottomCameraBorder;
    public Vector3 rightBottomCameraBorder;
    public bool randomBox;
    // Start is called before the first frame update
    void Start()
    {
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        pipe1UpOriginalTransform = pipe1Up.transform;
        pipe1DownOriginalTransform = pipe1Down.transform;
        movement = new Vector2(-1, 0);
        box1 = GameObject.FindGameObjectWithTag("box1");
        box2 = GameObject.FindGameObjectWithTag("box2");
    }

    // Update is called once per frame
    void Update()
    {
        pipe1Up.GetComponent<Rigidbody2D>().velocity = movement;	//	Déplacement	du	pipe	haut	
		pipe1Down.GetComponent<Rigidbody2D>().velocity = movement;	//	Déplacement	du	pipe	bas	
        box1.GetComponent<Rigidbody2D>().velocity = movement;	//	Déplacement	du	pipe	haut	
		box2.GetComponent<Rigidbody2D>().velocity = movement;	//	Déplacement	du	pipe	bas	
		siz.x =	pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x;	//	Récupera;on	de	la	taille	d’un	pipe		
		siz.y =	pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y;	//	Suffisant	car	ils ont	la	même taille
        
		//	Le pipe est sorti de l’écran ?	Si oui appel de la méthode moveTORightPipe
		if(pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)){
            moveToRightPipe();
        }
   
    }
    	void moveToRightPipe(){	
            float randomY = Random.Range(1,4) - 2;	//	Tirage aléatoire	d’un	décalage	en	Y
            
            float posX = rightBottomCameraBorder.x + (siz.x / 2);	//	Calcul	du	X	du	bord droite	de	l’écran
            //	Calcul	du	nouvel	Y	en	reprenant	la	position	Y	d’origine	du	pipe,	ici	le	downPipe1	
            float posY = pipe1UpOriginalTransform.position.y + randomY;	
            //	Création	du	vector3	contenant	la	nouvelle	position

            Vector3	tmpPos = new Vector3 (posX,	posY, pipe1Up.transform.position.z);	    
            // Affectationde ceVe nouvelle position au transform du gameObject
            pipe1Up.transform.position = tmpPos;

            float posY2 = posY;
            //	Idem pour le second	pipe	
            posY = pipe1DownOriginalTransform.position.y + randomY;

            tmpPos = new Vector3 (posX, posY, pipe1Down.transform.position.z);	
            pipe1Down.transform.position = tmpPos;
            
            //Recuperation de variable
            randomBox = Variable.getRandomBox();
            //On fait la somme des positions Y et il est divisé par 2
            Vector3	boxPos = new Vector3 (posX, ((posY + posY2) / 2) , -5);
            
            //Si c'est true il va afficher la premiere monnaie
            if (randomBox){
                
                box1.transform.position = boxPos;
            }else{
                
                box2.transform.position = boxPos;
            }
            //On change le valeur de la variable pour changer la position de la monniae et éviter changer les deux au même temps
            randomBox = !randomBox;

            //Envoyer le valeur changé au Singleton
            Variable.setRandomBox(randomBox);

		}
}
