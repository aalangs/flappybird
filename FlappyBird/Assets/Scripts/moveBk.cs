﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour {

	Vector2 movement = new Vector2(-2, 0);
	Vector3 siz;
	Vector3 leftBottomCameraBorder;
	float positonRestartX;

	// Use this for initialization
	void Start () {
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
	}
	
	// Update is called once par frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = movement;
		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;	
		siz.y = gameObject.GetComponent<SpriteRenderer>	().bounds.size.y;
		positonRestartX = siz.x;	

		if(transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)){
			transform.position = new Vector3(positonRestartX,transform.position.y,transform.position.z);
		}		
	}
}
