﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scoreFinal : MonoBehaviour
{
    //Variable pour faire le blink 
    public bool textBlink = false;
    // Start is called before the first frame update
    void Start()
    {
        //On affiche le score obtenu
        gameObject.GetComponent<UnityEngine.UI.Text>().text = Variable.getScore().ToString();

        //Si le score obtenu c'est plus grand que le Best Score on va le modifier, pour créer le nouveau New Best Score
        if(Variable.compareScore(Variable.getScore())){
            GameObject.FindGameObjectWithTag("bestScore").GetComponent<UnityEngine.UI.Text>().text =  "NEW BEST SCORE: "+Variable.getBestScore();
            //On fait la blink de Best Score
            textBlink = true;
        }else{
            //Sinon on affiche le Best Score jusqu'à ce moment
            GameObject.FindGameObjectWithTag("bestScore").GetComponent<UnityEngine.UI.Text>().text =  "BEST SCORE: "+Variable.getBestScore();
        }
        //On lance le blink
        StartCoroutine("FlashText");
    }

    public IEnumerator FlashText()
    {
        //Si c'est un nouveau New Best Score on fait le blink
        while (textBlink)
        {
            GameObject.FindGameObjectWithTag("bestScore").GetComponent<UnityEngine.UI.Text>().enabled = false;
            yield return new WaitForSeconds(.2f);
            GameObject.FindGameObjectWithTag("bestScore").GetComponent<UnityEngine.UI.Text>().enabled = true;
            yield return new WaitForSeconds(.2f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Si l'utilisateur touche n'importe quel lettre on lance la scene Menu
        if (Input.anyKeyDown)
        {
            //On recommence le score
            Variable.setScore(0);
            SceneManager.LoadScene("scene2-Menu");
        }
    }
}
