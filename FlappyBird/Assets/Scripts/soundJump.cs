﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class audioSound : MonoBehaviour
{
    AudioSource soundJump;
    AudioSource soundPoint;
    // Start is called before the first frame update
    void Start()
    {
        soundJump = GetComponent<AudioSource>();
        soundJump.Pause();

        soundPoint = GetComponent<AudioSource>();
        soundPoint.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            soundJump.Play();
        }
    }
}
