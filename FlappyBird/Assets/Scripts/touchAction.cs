﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class touchAction : MonoBehaviour
{
    public float speed;
    AudioSource soundJump;
    // Start is called before the first frame update
    void Start()
    {
        soundJump = GameObject.FindGameObjectWithTag("soundJump").GetComponent<AudioSource>();
        soundJump.Pause();
        speed = 4;
    }

    // Update is called once per frame
    void Update()
    {
        //Quand le bird fait le jump on lance le sound
        if (Input.anyKeyDown)
        {
            soundJump.Play();
            GetComponent<Rigidbody2D> ().velocity = new Vector2(0, speed);
        }
    }
}
