﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBkR : MonoBehaviour
{
    Vector2 movement = new Vector2(2, 0);
	Vector3 siz;
	Vector3 rightBottomCameraBorder;
	float positonRestartX;
    // Start is called before the first frame update
    void Start()
    {
        //leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        positonRestartX = GameObject.FindGameObjectWithTag("bk2").transform.position.x;
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        //leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        //rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
        siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        GetComponent<Rigidbody2D>().velocity = movement;	
    }

    // Update is called once per frame
    void Update()
    {
		if(transform.position.x > rightBottomCameraBorder.x + (siz.x/2)){
			transform.position = new Vector3(positonRestartX,transform.position.y,transform.position.z);
		}		
    }
}
