﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class collideManagementBird : MonoBehaviour
{   
    GameObject bird;
    GameObject score;
    float count = 0;
    AudioSource soundPoint;
    AudioSource soundHit;
    // Start is called before the first frame update
    void Start()
    {
        soundPoint = GameObject.FindGameObjectWithTag("soundPoint").GetComponent<AudioSource>();
        soundPoint.Pause();

        soundHit = GameObject.FindGameObjectWithTag("soundHit").GetComponent<AudioSource>();
        soundHit.Pause();

        bird = GameObject.FindGameObjectWithTag("bird");
        score = GameObject.FindGameObjectWithTag("score");

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D (Collider2D collider)
    {
        if (collider.gameObject.tag == "downPipe" || collider.gameObject.tag == "upPipe") 
        {
            //Si le bird fait la collision avec les pipes on lance le sound de hit
            soundHit.Play();
            //On lance le script de endAction pour rediriger à la scene Game Over
            bird.AddComponent<endAction>();
        }

        if (collider.gameObject.tag == "box1" || collider.gameObject.tag == "box2")
        {
            //Si le bird touch la monnaie on lance le sound du point
            soundPoint.Play();
            //On fait un count pour incrementer le score chaque fois quand se touche la monniae
            count = Variable.getScore();
            count++;
            //On affiche le nouveau score dans la scene du jeu
            score.GetComponent<UnityEngine.UI.Text>().text = count.ToString();
            //On envoie la nouveau score
            Variable.setScore(count);
            //Debug.Log(count);
        }

        //Si le bird sort de la scene on lance la scene Game Over
        if (!gameObject.GetComponent<SpriteRenderer> ().isVisible)
        {
            bird.AddComponent<endAction>();
        }
    }
}